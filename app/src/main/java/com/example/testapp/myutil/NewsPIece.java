package com.example.testapp.myutil;

import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

public class NewsPIece{
    public String title, text, date, img;
    public Bitmap picture;

    public NewsPIece(JSONObject object) throws JSONException {
        this.title = object.getString("tittle");
        this.text = object.getString("text");
        this.date = object.getString("date");
        this.img = object.getString("img");
    }

}
