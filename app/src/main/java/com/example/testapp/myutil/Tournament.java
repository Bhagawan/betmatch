package com.example.testapp.myutil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Tournament {
    public String name;
    public ArrayList<Game> games = new ArrayList<>();

    public class Game {
        public String Team,goals,percent,amount,won,draw,loss,points;
        public Game(JSONObject jsonObject) throws JSONException {
            this.Team = jsonObject.getString("Команда");
            this.amount = String.valueOf(jsonObject.getInt("Игры"));
            this.won = String.valueOf(jsonObject.getInt("В"));
            this.draw = String.valueOf(jsonObject.getInt("Н"));
            this.loss = String.valueOf(jsonObject.getInt("П"));
            this.goals = jsonObject.getString("Мячи");
            this.points = String.valueOf(jsonObject.getInt("Очки"));
            this.percent = jsonObject.getString("% очков");
        }
    }
    public Tournament(JSONObject jsonObject) throws JSONException {
        this.name = jsonObject.getString("name");
        int i = jsonObject.getInt("players_count");
        JSONObject data = jsonObject.getJSONObject("data");
        for(int j = 1; j<=i; j++) {
            games.add(new Game(data.getJSONObject(String.valueOf(j))));
        }
    }
}