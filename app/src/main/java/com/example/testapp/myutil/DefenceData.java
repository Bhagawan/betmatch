package com.example.testapp.myutil;

import org.json.JSONException;
import org.json.JSONObject;

public class DefenceData {
    public String team, tournament, hits, taken;

    public DefenceData (JSONObject jsonObject) throws JSONException {
        this.team = jsonObject.getString("Команда");
        this.tournament = jsonObject.getString("Турнир");
        this.hits = String.valueOf(jsonObject.getInt("Удары з.и."));
        this.taken = String.valueOf(jsonObject.getInt("Отборы з.и."));
    }
}
