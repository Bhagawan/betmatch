package com.example.testapp.myutil;

import org.json.JSONException;
import org.json.JSONObject;

public class AttackData {
    public String team, tournament, hits, hitsInGate;

    public AttackData (JSONObject jsonObject) throws JSONException {
        this.team = jsonObject.getString("Команда");
        this.tournament = jsonObject.getString("Турнир");
        this.hits = String.valueOf(jsonObject.getInt("Удары з.и."));
        this.hitsInGate = String.valueOf(jsonObject.getInt("Удары ВСтв з.и."));
    }
}
