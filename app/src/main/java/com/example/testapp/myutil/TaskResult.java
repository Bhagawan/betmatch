package com.example.testapp.myutil;

import org.json.JSONArray;
import org.json.JSONException;

public interface TaskResult {
    void handleResult(JSONArray output) throws JSONException;
}
