package com.example.testapp.myutil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StatData {
    public static ArrayList<DefenceData> defenceStat = new ArrayList<>();
    public static ArrayList<AttackData> attackStat = new ArrayList<>();

    public static void setDefenceStat(JSONArray array) throws JSONException {
        for(int i = 0; i<array.length(); i++){
            defenceStat.add(new DefenceData((JSONObject) array.get(i)));
        }
    }
    public static void setAttackStat(JSONArray array) throws JSONException {
        for(int i = 0; i<array.length(); i++){
            attackStat.add(new AttackData((JSONObject) array.get(i)));
        }
    }

}
