package com.example.testapp.myutil;

public interface SettingsChanger {
    void changeBackground();
    void changeTheme();
}
