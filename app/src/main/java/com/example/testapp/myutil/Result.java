package com.example.testapp.myutil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Result {
    public static Tournament[] tournaments = new Tournament[4];

    public Result(JSONArray jsonArray) throws JSONException {
        for (int i = 0; i<4; i++) {
            tournaments[i] = new Tournament((JSONObject) jsonArray.get(i));
        }
    }
    public static void setResult(JSONArray jsonArray) throws JSONException {
        for (int i = 0; i<4; i++) {
            tournaments[i] = new Tournament((JSONObject) jsonArray.get(i));
        }
    }
}
