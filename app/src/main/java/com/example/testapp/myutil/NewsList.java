package com.example.testapp.myutil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NewsList {
    public static ArrayList<NewsPIece> news = new ArrayList<>();

    public static void setNews(JSONArray array) throws JSONException {
        for(int i = 0; i<array.length(); i++){
            news.add(new NewsPIece((JSONObject) array.get(i)));
        }
    }

}
