package com.example.testapp.myutil;

import android.graphics.Bitmap;

public interface GetBitmap {
    void handleBitmap(Bitmap picture);
}
