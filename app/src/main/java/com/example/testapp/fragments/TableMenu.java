package com.example.testapp.fragments;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.testapp.R;
import com.example.testapp.myutil.MyJsonUtil;
import com.example.testapp.myutil.Result;
import com.example.testapp.myutil.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;

public class TableMenu extends Fragment implements TaskResult {

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Navigation.findNavController(requireActivity(),R.id.fragmentContainerView2).navigate(R.id.menu);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.table_menu_layout, container, false);
        Button russiaButton = (Button) view.findViewById(R.id.russia_button);
        MyJsonUtil Task = new MyJsonUtil();
        Task.result = this;
        Task.execute("http://84.38.181.162/api/data_tournament_tables.json");

        russiaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("title","russia");
                Navigation.findNavController(v).navigate(R.id.tableOutput,bundle);
            }
        });
        Button englandButton = (Button) view.findViewById(R.id.england_button);
        englandButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("title","england");
                Navigation.findNavController(v).navigate(R.id.tableOutput,bundle);
            }
        });
        Button germanyButton = (Button) view.findViewById(R.id.germany_button);
        germanyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("title","germany");
                Navigation.findNavController(v).navigate(R.id.tableOutput,bundle);
            }
        });
        Button spainButton = (Button) view.findViewById(R.id.spain_button);
        spainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("title","spain");
                Navigation.findNavController(v).navigate(R.id.tableOutput,bundle);
            }
        });
        return view;
    }

    @Override
    public void handleResult(JSONArray output) throws JSONException {
        if(output !=null )
            Result.setResult(output);
        else Toast.makeText(getContext(),"Интернет недоступен.",Toast.LENGTH_SHORT).show();
    }
}