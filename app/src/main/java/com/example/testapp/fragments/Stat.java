package com.example.testapp.fragments;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.testapp.R;
import com.example.testapp.myutil.MyJsonUtil;
import com.example.testapp.myutil.StatData;
import com.example.testapp.myutil.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;

public class Stat extends Fragment{
    private View view;
    private boolean attackisOn = false;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Navigation.findNavController(requireActivity(),R.id.fragmentContainerView2).navigate(R.id.menu);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.stat_layout, container, false);
        TextView textView = view.findViewById(R.id.stat_layout_hedliner);
        textView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        setDefence();
        Button attackButton = view.findViewById(R.id.stat_attack_button);
        Button defenceButton = view.findViewById(R.id.stat_defence_button);
        attackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!attackisOn) {
                    defenceButton.setBackgroundResource(R.drawable.dark_button);
                    attackButton.setBackgroundResource(R.drawable.button_shape);
                    setAttack();
                    attackisOn = true;
                }
            }
        });
        defenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(attackisOn) {
                    defenceButton.setBackgroundResource(R.drawable.button_shape);
                    attackButton.setBackgroundResource(R.drawable.dark_button);
                    setDefence();
                    attackisOn = false;
                }
            }
        });

        return view;
    }

    private void setAttack() {
        if(StatData.attackStat.size() == 0) {
            MyJsonUtil task = new MyJsonUtil();
            task.result = new TaskResult() {
                @Override
                public void handleResult(JSONArray output) throws JSONException {
                    if(output != null){
                        StatData.setAttackStat(output);
                        fillAttack();
                    }
                    else Toast.makeText(getContext(),"Интернет недоступен.",Toast.LENGTH_SHORT).show();
                }
            };
            task.execute("http://84.38.181.162/api/data_statistic_attack.json");
        }
        else fillAttack();
    }
    private void setDefence() {
        if(StatData.defenceStat.size() == 0) {
            MyJsonUtil task = new MyJsonUtil();
            task.result = new TaskResult() {
                @Override
                public void handleResult(JSONArray output) throws JSONException {
                    if(output != null) {
                        StatData.setDefenceStat(output);
                        fillDefence();
                    }
                    else Toast.makeText(getContext(),"Интернет недоступен.",Toast.LENGTH_SHORT).show();
                }
            };
            task.execute("http://84.38.181.162/api/data_statistic_defense.json");
        }
        else fillDefence();
    }
    private void fillAttack() {
        TableLayout tableLayout = view.findViewById(R.id.stat_output);
        tableLayout.removeAllViews();
        TableRow tr = new TableRow(view.getContext());
        tr.setBackgroundColor(getResources().getColor(R.color.black));

        TextView text1 = new TextView(view.getContext());
        text1.setText("Команда");
        text1.setTextColor(getResources().getColor(R.color.white));
        tr.addView(text1, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,2.0f));

        TextView text2 = new TextView(view.getContext());
        text2.setText("Турнир");
        text2.setTextColor(getResources().getColor(R.color.white));
        tr.addView(text2, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,2.0f));

        TextView text3 = new TextView(view.getContext());
        text3.setText("Удары з.и.");
        text3.setTextColor(getResources().getColor(R.color.white));
        tr.addView(text3, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

        TextView text4 = new TextView(view.getContext());
        text4.setText("Удары ВСтв з.и.");
        text4.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        text4.setTextColor(getResources().getColor(R.color.white));
        tr.addView(text4, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

        tableLayout.addView(tr, new TableRow.LayoutParams (TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.MATCH_PARENT));
        if(StatData.attackStat!=null)
            for( int i = 0; i < StatData.attackStat.size(); i++) {
                TableRow row = new TableRow(view.getContext());

                TextView team = new TextView(view.getContext());
                team.setText(StatData.attackStat.get(i).team);
                team.setTextColor(getResources().getColor(R.color.black));
                row.addView(team, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,2.0f));

                TextView tournament = new TextView(view.getContext());
                tournament.setText(StatData.attackStat.get(i).tournament);
                tournament.setTextColor(getResources().getColor(R.color.black));
                row.addView(tournament, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,2.0f));

                TextView hits = new TextView(view.getContext());
                hits.setText(StatData.attackStat.get(i).hits);
                hits.setTextColor(getResources().getColor(R.color.black));
                row.addView(hits, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

                TextView goalHits = new TextView(view.getContext());
                goalHits.setText(StatData.attackStat.get(i).hitsInGate);
                goalHits.setTextColor(getResources().getColor(R.color.black));
                row.addView(goalHits, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

                tableLayout.addView(row, new TableRow.LayoutParams (TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.MATCH_PARENT));

            }
    }
    private void fillDefence() {
        TableLayout tableLayout = view.findViewById(R.id.stat_output);
        tableLayout.removeAllViews();
        TableRow tr = new TableRow(view.getContext());
        tr.setBackgroundColor(getResources().getColor(R.color.black));

        TextView text1 = new TextView(view.getContext());
        text1.setText("Команда");
        text1.setTextColor(getResources().getColor(R.color.white));
        tr.addView(text1, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,2.0f));

        TextView text2 = new TextView(view.getContext());
        text2.setText("Турнир");
        text2.setTextColor(getResources().getColor(R.color.white));
        tr.addView(text2, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,2.0f));

        TextView text3 = new TextView(view.getContext());
        text3.setText("Удары з.и.");
        text3.setTextColor(getResources().getColor(R.color.white));
        tr.addView(text3, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

        TextView text4 = new TextView(view.getContext());
        text4.setText("Перехваты з.и.");
        text4.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        text4.setTextColor(getResources().getColor(R.color.white));
        tr.addView(text4, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

        tableLayout.addView(tr, new TableRow.LayoutParams (TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.MATCH_PARENT));
        if(StatData.defenceStat!=null)
            for( int i = 0; i < StatData.defenceStat.size(); i++) {
                TableRow row = new TableRow(view.getContext());

                TextView team = new TextView(view.getContext());
                team.setText(StatData.defenceStat.get(i).team);
                team.setTextColor(getResources().getColor(R.color.black));
                row.addView(team, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,2.0f));

                TextView tournament = new TextView(view.getContext());
                tournament.setText(StatData.defenceStat.get(i).tournament);
                tournament.setTextColor(getResources().getColor(R.color.black));
                row.addView(tournament, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,2.0f));

                TextView hits = new TextView(view.getContext());
                hits.setText(StatData.defenceStat.get(i).hits);
                hits.setTextColor(getResources().getColor(R.color.black));
                row.addView(hits, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

                TextView taken = new TextView(view.getContext());
                taken.setText(StatData.defenceStat.get(i).taken);
                taken.setTextColor(getResources().getColor(R.color.black));
                row.addView(taken, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

                tableLayout.addView(row, new TableRow.LayoutParams (TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.MATCH_PARENT));

            }
    }
}
