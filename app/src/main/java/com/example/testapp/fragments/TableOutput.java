package com.example.testapp.fragments;
import androidx.fragment.app.Fragment;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testapp.R;
import com.example.testapp.myutil.Result;

public class TableOutput extends Fragment{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.table_display, container, false);
        assert getArguments() != null;
        String title = getArguments().getString("title");
        TextView textView = view.findViewById(R.id.textView);
        textView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        int n = 0;
        switch (title) {
            case "russia" :
                textView.setText(getResources().getString(R.string.russia));
                n=1;
                break;
            case "england" :
                textView.setText(getResources().getString(R.string.england));
                break;
            case "germany" :
                textView.setText(getResources().getString(R.string.germany));
                n=3;
                break;
            case "spain" :
                textView.setText(getResources().getString(R.string.spain));
                n=2;
                break;
        }

        TableLayout tableLayout = view.findViewById(R.id.table_output);
        if(Result.tournaments[0].name != null) {
            for (int i = 0; i < Result.tournaments[n].games.size(); i++) {
                TableRow tr = new TableRow(view.getContext());

                TextView number = new TextView(view.getContext());
                number.setText(String.valueOf(i + 1));
                tr.addView(number, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

                TextView team = new TextView(getContext());
                team.setText(Result.tournaments[n].games.get(i).Team);
                tr.addView(team, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,10.0f));

                TextView amount = new TextView(getContext());
                amount.setText(Result.tournaments[n].games.get(i).amount);
                tr.addView(amount, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

                TextView win = new TextView(getContext());
                win.setText(Result.tournaments[n].games.get(i).won);
                tr.addView(win, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

                TextView draw = new TextView(getContext());
                draw.setText(Result.tournaments[n].games.get(i).draw);
                tr.addView(draw, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

                TextView lost = new TextView(getContext());
                lost.setText(Result.tournaments[n].games.get(i).loss);
                tr.addView(lost, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

                TextView goals = new TextView(getContext());
                goals.setText(Result.tournaments[n].games.get(i).goals);
                tr.addView(goals, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,2.0f));

                TextView points = new TextView(getContext());
                points.setText(Result.tournaments[n].games.get(i).points);
                tr.addView(points, new TableRow.LayoutParams (0,TableRow.LayoutParams.MATCH_PARENT,1.0f));

                tableLayout.addView(tr, new TableRow.LayoutParams (TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.MATCH_PARENT));
            }
        } else Toast.makeText(getContext(),"Интернет недоступен.",Toast.LENGTH_SHORT).show();

        return  view;
    }

}