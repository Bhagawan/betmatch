package com.example.testapp.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.testapp.R;
import com.example.testapp.myutil.GetBitmap;
import com.example.testapp.myutil.MyJsonUtil;
import com.example.testapp.myutil.NewsList;
import com.example.testapp.myutil.PictureDownloader;
import com.example.testapp.myutil.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;

public class News extends Fragment implements TaskResult{
    private View view;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Navigation.findNavController(requireActivity(),R.id.fragmentContainerView2).navigate(R.id.menu);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.news_layout, container, false);
        if(NewsList.news.size() == 0) {
            MyJsonUtil task = new MyJsonUtil();
            task.result = this;
            task.execute("http://84.38.181.162/api/news.json");
        }
        else {
            fillScroll();
        }
        TextView t = view.findViewById(R.id.news_layout_hedliner);
        t.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        return view;
    }

    @Override
    public void handleResult(JSONArray output) throws JSONException {
        if(output != null) {
            NewsList.setNews(output);
            fillScroll();
            waitForPictures();
        }
        else Toast.makeText(getContext(),"Интернет недоступен.",Toast.LENGTH_SHORT).show();
    }

    private void waitForPictures() {
       for(int i = 0; i < NewsList.news.size(); i++) {
           addPicture(i);
       }
    }

    private void fillScroll() {
        ScrollView scrollView = view.findViewById(R.id.news_output);
        scrollView.removeAllViews();
        LinearLayout linearLayout = new LinearLayout(view.getContext());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setBackgroundColor(getResources().getColor(R.color.white));
        if( NewsList.news != null)
        for(int i = 0; i < NewsList.news.size(); i++) {
            LinearLayout ll = new LinearLayout(view.getContext());
            ll.setOrientation(LinearLayout.VERTICAL);
            ll.setPadding(10,20,10,0);

            if(NewsList.news.get(i).picture!=null) {
                ImageView iv = new ImageView(view.getContext());
                iv.setImageBitmap(NewsList.news.get(i).picture);
                ll.addView(iv,new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
            }


            TextView headliner = new TextView(view.getContext());
            headliner.setText(NewsList.news.get(i).title);
            headliner.setTextSize(20);
            headliner.setTextColor(getResources().getColor(R.color.black));
            ll.addView(headliner,new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));

            TextView data = new TextView(view.getContext());
            data.setText(NewsList.news.get(i).date);
            data.setTextColor(getResources().getColor(android.R.color.darker_gray));
            ll.addView(data,new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));

            TextView text = new TextView(view.getContext());
            text.setText(NewsList.news.get(i).text);
            text.setTextColor(getResources().getColor(android.R.color.darker_gray));
            ll.addView(text,new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));

            linearLayout.addView(ll,new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        }
        scrollView.addView(linearLayout,new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addPicture(int i) {
        PictureDownloader p = new PictureDownloader();
        p.result = new GetBitmap() {
            @Override
            public void handleBitmap(Bitmap picture) {
                NewsList.news.get(i).picture = picture;
                fillScroll();
            }
        };
        p.execute(NewsList.news.get(i).img);
    }
}
